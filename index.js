const express = require("express");

// Mongoose is a package that allows us to create Schemas to model our data structures and to manipulate our database using different access methods
const mongoose = require("mongoose");

const port = 3001;

/*
// [SECTION] MongoDB Connection

syntax:
mongoose.connect("<MongoDB Atlas Connection String>",
	{
		//Allow us to avoid any current and future errors while connecting to MongoDB
		useNewUrlParser: true,
		useUnifiedTopology: true

	}
);

*/
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.o8syqdi.mongodb.net/b203_to-do?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", ()=>console.log("We're connected to the cloud database"));



const app = express();


/*
[SECTION] Mongoose Schemas
schemas determine the structure of the documents to be written in the database
schemas act as blueprints to our data

syntax:

const schemaName = new monoose.Schema({<keyvalue:pair>});
*/
// name & status
// "required" is used to specify that a field must not be empty
// "default" is used if a field value is not supplied
const taskSchema = new mongoose.Schema({
	name: {
		type: String,
		required:[true, "Task name is required"]
	},
	status:
	{
		type: String,
		default: "pending"
	}

})




/*
[SECTION] Models

uses schema and use it to create / instantiate documents / object that follows our schema structure.

the variable / object that will be created can be used to run commands for interacting with our database.

syntax

const VariableName = mongoose.model("collectionName", schemaName);




*/

const Task = mongoose.model("Task", taskSchema);



// middlewares
app.use(express.json());// Allow app to read json data

app.use(express.urlencoded({extended:true}));//Allows your app to read data from forms


// Business Logic
/*
    1. Add a functionality to check if there are duplicate tasks
        - If the task already exists in the database, we return an error message "Duplicate task found"
        - If the task doesn't exist in the database, we add it in the database
        A task doesn't exists if:
            - result from the query is not null
            - result.name is equal to req.body.name
    2. The task data will be coming from the request's body
    3. Create a new Task object with a "name" field/property
    4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

	app.post("/tasks", (req, res)=>{
		console.log(req.body.name);
		// "findOne" is a Mongoose method that acts similar to "find" of MongoDB
		// findOne() returns the first document that matches the search criteria
			// If there are no matches, the value of result is null
			// "err" is a shorthand naming convention for errors.
		Task.findOne({name: req.body.name}, (err, result) => {
			console.log(result);

			if(result !== null && result.name === req.body.name){
				return res.send("Duplicate task found!");
			}
			else {
					// "newTask" was created/instantiaited from the Mongoose schema and will gain access to ".save" method
				let newTask = new Task({
					name: req.body.name
				});

				newTask.save((saveErr, savedTask) => {
					if(saveErr){
						return console.error(saveErr)
					}
					else{
						return res.status(201).send("New Task created!");
					}
				})
			}
		})
	});

	// Getting all the tasks

	// Business Logic
	/*
		1. Retrieve all the documents
		2. If an error is encountered, print the error
		3. If no errors are found, send a success status back to the client/Postman and return an array of documents
	*/
	app.get("/tasks", (req,res) =>{
		Task.find({name: "eating"}, (err, result) =>{
			if(err){
				return console.log(err);
			}
			else{
				return res.status(200).send({
					Tasks: result
				})
			}
		})
	})





// Getting all the tasks

    // Business Logic
    /*
        1. Retrieve all the documents
        2. If an error is encountered, print the error
        3. If no errors are found, send a success status back to the client/Postman and return an array of documents
    */
/*
                Mini Activity
                    1. Create a User schema.
                        username - string
                        password - string
                    2. Create a User model.
            */




const userSchema = new mongoose.Schema({
	username: {
		type: String,
		required:[true, "a user name is required"]
	},
	password:
	{
		type: String,
		required:[true, "a password is required"]
	}

})



const User = mongoose.model("User", userSchema);



	app.post("/signup", (req, res)=>{
		User.findOne({username: req.body.username}, (err, result) => {
			console.log(result);

			if(result !== null && result.username === req.body.username){
				return res.send("Duplicate User found!");
			}
			else {
				let newUser = new User({
					username: req.body.username,
					password: req.body.password,
				});

				newUser.save((saveErr, savedUser) => {
					if(saveErr){
						return console.error(saveErr)
					}
					else{
						return res.status(201).send("New User created!");
						
					}
				})
			}
		})
	});



// Adding of new user

/*
Instructions s35 Activity:
1. Using User model.
2. Create a POST route that will access the "/signup" route that will create a user.
5. Process a POST request at the "/signup" route using postman to register a user.
6. Create a git repository named S35.
7. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
8. Add the link in Boodle.
*/





app.listen(port, ()=> console.log(`Server running at port ${port}`));

